﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("4.3.7.1")]
[assembly: AssemblyFileVersion("4.3.7.1")]
[assembly: AssemblyInformationalVersion("4.3.7.1")]

[assembly: AssemblyCompany("GHI Electronics, LLC")]
[assembly: AssemblyCopyright("Copyright © GHI Electronics, LLC 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]